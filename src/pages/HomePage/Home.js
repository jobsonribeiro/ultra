import React from 'react';
import { homeObjOne,
         homeObjTwo,
         homeObjThree,
         homeObjFour } from './Data';
import { InfoSection, Pricing } from '../../components/';
import MainContent from '../../components/Main/MainContent';



const Home = () => {
    return (
        <>
            <MainContent />
            <InfoSection {...homeObjOne}/>
            <Pricing />
            <InfoSection {...homeObjTwo}/>
            <InfoSection {...homeObjThree}/>
            <InfoSection {...homeObjFour}/>
        </>
    )
}

export default Home
