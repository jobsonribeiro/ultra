export const homeObjOne = {
    lightBg: false,
    primary: true,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
    buttonLabel: 'Get Started', 
    address: 'Endereço: Rodovia BR 230 Km 14, s/n, Morada Nova, PB, 58109-3030',
    description: 'Como confirmação de nosso compromisso essencial com a democratização do acesso ao Ensino Superior de qualidade, a unificação de toda a nossa rede de Faculdades e Centros Universitários sob a direção de uma única organização mantenedora, projeta-se como marco no cenário da Educação no Brasil pelo que já fizemos até aqui e pelo muito que nos dispomos a fazer pela formação profissional, humana, ética e cultural da juventude de nosso país.',
    headLine: 'UNIESP', 
    lightText: true, 
    topLine: 'Local do Evento',
    img: require('../../images/uniesp.jpg').default,
    alt: 'Image',
    start: ''
}

export const homeObjTwo = {
    lightBg: true,
    primary: false,
    imgStart:'start',
    lightTopLine: false, 
    lightTextDesc: false,
    buttonLabel: 'Get Started',
    address: '', 
    description: 'We help business owners increase their revenue. Our team of unique specialist can help you achieve business goals',
    headLine: 'Lead Generation Specialist for Online Businesses', 
    lightText: false, 
    topLine: 'Marketing Agency',
    img: require('../../images/profile-1.jpg').default,
    alt: 'Image',
    start: 'true'
}

export const homeObjThree = {
    lightBg: false,
    primary: true,
    imgStart:'',
    lightTopLine: true, 
    lightTextDesc: true,
    buttonLabel: 'Get Started', 
    address: '',
    description: 'We help business owners increase their revenue. Our team of unique specialist can help you achieve business goals',
    headLine: 'Lead Generation Specialist for Online Businesses', 
    lightText: true, 
    topLine: 'Marketing Agency',
    img: require('../../images/svg-2.svg').default,
    alt: 'Image',
    start: ''
}

export const homeObjFour = {
    lightBg: false,
    primary: true,
    imgStart:'start',
    lightTopLine: true, 
    lightTextDesc: true,
    buttonLabel: 'Get Started', 
    address: '',
    description: 'We help business owners increase their revenue. Our team of unique specialist can help you achieve business goals',
    headLine: 'Lead Generation Specialist for Online Businesses', 
    lightText: true, 
    topLine: 'Marketing Agency',
    img: require('../../images/svg-3.svg').default,
    alt: 'Image',
    start: 'true'
}