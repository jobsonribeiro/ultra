import React from 'react';
import { Button } from '../../globalStyles';
import { GiCrystalBars } from 'react-icons/gi';
import { GiCutDiamond /* GiRock */ } from 'react-icons/gi';
import { IconContext } from 'react-icons/lib';
import {
  PricingSection,
  PricingWrapper,
  PricingHeading,
  PricingContainer,
  PricingCard,
  PricingCardInfo,
  PricingCardIcon,
  PricingCardPlan,
  PricingCardCost,
  PricingCardLength,
  PricingCardFeatures,
  PricingCardFeature,
  Img1,
  Img2,
  Img3
} from './Pricing.elements';

import patrocinador1 from '../../images/patrocinador1.png';
import patrocinador2 from '../../images/patrocinador2.png';
import patrocinador3 from '../../images/patrocinador3.png';


function Pricing() {
  return (
    <IconContext.Provider value={{ color: '#a9b3c1', size: 64 }}>
      <PricingSection>
        <PricingWrapper>
          <PricingHeading>Patrocinadores</PricingHeading>
          <PricingContainer>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img1 src={patrocinador1} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img2 src={patrocinador2} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={patrocinador3} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
          </PricingContainer>
          <PricingContainer>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img1 src={patrocinador1} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img2 src={patrocinador2} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            <PricingCard>
              <PricingCardInfo>
                <PricingCardIcon>
                  <Img3 src={patrocinador3} />
                  {/* <GiRock /> */}
                </PricingCardIcon>
              </PricingCardInfo>
            </PricingCard>
            
          </PricingContainer>
        </PricingWrapper>
      </PricingSection>
    </IconContext.Provider>
  );
}
export default Pricing;