import React from 'react';
import { Button } from '../../globalStyles';
import { GiCrystalBars } from 'react-icons/gi';
import { GiCutDiamond /* GiRock */ } from 'react-icons/gi';
import { IconContext } from 'react-icons/lib';
import {
  PricingSection,
  PricingWrapper,
  PricingHeading,
  PricingContainer,
  TextWrapper,
  TopLine,
  Heading,
  Subtitle
} from './MainContent.elements'




function MainContent() {
  return (
    <IconContext.Provider value={{ color: '#a9b3c1', size: 64 }}>
      <PricingSection>
        <PricingWrapper>
          <PricingHeading>2ª EDIÇÃO DO EVENTO CONAMDRACON</PricingHeading>
          <PricingContainer>
          <TextWrapper>
            <TopLine> 
              O Evento Nos dias 03, 04 e 05 de novembro de 2021, a cidade de João Pessoa, no Estado da Paraíba, irá sediar um dos mais importantes eventos de Doenças Raras e Anomalias Congênitas, realizado no Brasil. Um encontro que mobilizará profissionais, pesquisadores e estudantes de diversas áreas, oriundos de diversas regiões do Brasil e do Exterior.
            </TopLine>
            <TopLine>
              O evento contará com palestrantes de renome nacional e internacional, instituições de ensino da região, associações de classe, além de importantes empresas do setor.
              O objetivo desse evento é alavancar e fortalecer os pesquisadores e profissionais de alta performance no país, facilitando a acessibilidade de Profissionais e acadêmicos da saúde e áreas afins a um evento de elevado nível científico e de logística, compatível com os melhores congressos de especialidades realizados nos grandes centros do Brasil e do mundo.
            </TopLine>
            <TopLine>
              Além disso, tem o objetivo de aproximar pesquisadores, profissionais, estudantes, universidades, empresas, entre outras instituições ligadas ao estudo, a pesquisa e a assistência no âmbito das Doenças Raras e Anomalias Congênitas.  
            </TopLine>
            <TopLine>
              Tema Central: O tema central do I CONAMDRACON será “Doenças raras e Anomalias Congênitas: desafios na busca pelo cuidado integral”.
            </TopLine>
            <TopLine>
              Baseado nessa temática, serão discutidos nesse evento, as novas condutas, além de protocolos clínicos e científicos, com o objetivo de usufruir dos novos recursos tecnológicos disponíveis nas diversas especialidades, na busca de tratamentos mais previsíveis a longo prazo.
            </TopLine>
          </TextWrapper>                      
          </PricingContainer>
        </PricingWrapper>
      </PricingSection>
    </IconContext.Provider>
  );
}
export default MainContent;